#version 330

// this was going to be the default shader to display if the first shader
// to load fails to compile, but it seemed way cooler in my head

uniform float delta;
uniform float elapsed;
uniform ivec2 screen;
uniform ivec2 mouse;

layout(location = 0) out vec4 frag_color;

void main() {
    vec2 uv = gl_FragCoord.xy / vec2(screen);

    float s = 0.5 + 0.5 * sin(-2.0 * elapsed + 5.0 * uv.x);
    float t = smoothstep(s-0.1, s+0.1, uv.y);

    vec3 color = mix(vec3(1.0), vec3(uv, 1.0), t);

    frag_color = vec4(color, 1.0);
}
