# Shader Viewer

Desktop shader viewer based on SDL2 and OpenGL.

## Usage

### Cli

```
Usage: shader-viewer [OPTIONS] [file]

Arguments:
  [file]

Options:
  -a, --always-on-top               Windows should always be drawn on top of others
  -t, --time-interveal <interveal>  Seconds to query for file updates [default: 1]
  -c, --context-version <version>   OpenGL context version (>= 3.3) [default: 3.3]
  -w, --window-title <title>        Window title [default: "Shader Viewer"]
  -s, --size <size>                 Window size [default: 800x800]
  -h, --help                        Print help
```

The file must be a valid glsl fragment shader that writes a `vec4` to `location 0`.

The [builtin fragment shader](src/shaders/builtin.frag) also serves an example of how a valid shader could look like.

This app uses [`log`](https://docs.rs/log/latest/log/) and [`env_logger`](https://docs.rs/env_logger/latest/env_logger/). It prints error messages (*i.e. shader compile errors*) by default but setting the `RUST_LOG` env variable will also enable other kinds of output.

### Desktop

It's also valid to open the app by opening the executable directly on a graphical session (or using the cli without any arguments), in this case a default shader will be displayed, to start watching a custom shader just drag the file on top of the window.

## Uniforms

- `delta: float` = *time between previous and current frame*
- `elapsed: float` = *total time elapsed since startup*
- `screen: ivec2` = *screen size in pixels*
- `mouse: ivec2` = *mouse position relative to the top-left corner of the window*

They all appear in the [builtin fragment shader](src/shaders/builtin.frag) and some of them are also used.

## Supported Features

- Hot reloading on file write
- File drop-in to watch a different shader

Hot reloading relies on checking for file last update metadata, however some text editors don't write to the file "directly" but "delete and rewrite" them instead *i.e. vim, gedit*, which won't work with hot reloading.

## Wanted features

- Textures
- Custom vertex shader
- A simpler fragment shader template
- ~~An option to have the window always on top (windowing crate limitation)~~
