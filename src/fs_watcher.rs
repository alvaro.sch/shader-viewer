use std::{
    fs::File,
    io,
    path::Path,
    time::{Duration, Instant, SystemTime},
};

#[derive(Debug)]
pub struct FileWatcher {
    file: Option<File>,

    watch_interval: Duration,
    last_watch: Instant,

    last_update: SystemTime,
}

impl FileWatcher {
    pub fn new<P>(path: Option<P>, watch_interval: Duration) -> Result<Self, io::Error>
    where
        P: AsRef<Path>,
    {
        let filepath = path.map(|p| p.as_ref().to_path_buf());
        let file = filepath.map(File::open).transpose()?;

        let last_update = SystemTime::now();

        let last_watch = Instant::now();

        Ok(Self {
            file,
            watch_interval,
            last_watch,
            last_update,
        })
    }

    pub fn swap_file<P>(&mut self, new_path: P) -> Result<(), io::Error>
    where
        P: AsRef<Path>,
    {
        let new_file = File::open(new_path)?;

        self.file = Some(new_file);
        self.last_watch = Instant::now();

        Ok(())
    }

    pub fn file_updated(&mut self) -> bool {
        let Some(ref file) = self.file else {
            return false;
        };

        let now = Instant::now();
        let mut modified = false;

        if now.duration_since(self.last_watch) > self.watch_interval {
            let Ok(last_file_update) = file.metadata().and_then(|metadata| metadata.modified())
            else {
                return false;
            };

            if last_file_update > self.last_update {
                self.last_update = last_file_update;
                modified = true;
            }

            self.last_watch = now;
        }

        modified
    }

    pub fn file_handle(&mut self) -> Option<&mut File> {
        self.file.as_mut()
    }

    pub fn watch_interval(&self) -> &Duration {
        &self.watch_interval
    }

    pub fn last_update(&self) -> &SystemTime {
        &self.last_update
    }
}
