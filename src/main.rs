use shader_viewer::{cli, App};

fn main() {
    env_logger::init();

    let args = cli::Args::parse();

    App::new(&args).run();
}
