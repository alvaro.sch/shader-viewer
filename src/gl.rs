pub mod program;
pub mod shader;
pub mod shader_bundle;
pub mod vertex_array;

//
// re-exports
//

pub use self::{
    program::Program, shader::Shader, shader_bundle::ShaderBundle, vertex_array::VertexArray,
};

//
// initialization and function wrappers
//

use sdl2::video::Window;

pub fn load(window: &Window) {
    gl::load_with(|proc| window.subsystem().gl_get_proc_address(proc) as *const _);
}

pub fn viewport(x: i32, y: i32, width: i32, height: i32) {
    unsafe { gl::Viewport(x, y, width, height) }
}
