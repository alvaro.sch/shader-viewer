use std::path::PathBuf;

use clap::{
    builder::TypedValueParser,
    error::{ContextKind, ContextValue, ErrorKind},
    Parser,
};

#[derive(Parser)]
pub struct Args {
    #[arg(value_name = "file")]
    pub watch: Option<PathBuf>,

    #[arg(short, long, default_value_t = false)]
    #[arg(help = "Windows should always be drawn on top of others")]
    pub always_on_top: bool,

    #[arg(short, long, default_value_t = 1, value_name = "interveal")]
    #[arg(help = "Seconds to query for file updates")]
    pub time_interveal: u64,

    #[arg(short, long, default_value = "3.3", value_name = "version", value_parser = ContextVersionParser)]
    #[arg(help = "OpenGL context version (>= 3.3)")]
    pub context_version: ContextVersion,

    #[arg(short, long, default_value = "Shader Viewer", value_name = "title")]
    #[arg(help = "Window title")]
    pub window_title: String,

    #[arg(short, long, default_value = "800x800", value_name = "size", value_parser = ScreenSizeParser)]
    #[arg(help = "Window size")]
    pub size: ScreenSize,
}

impl Args {
    pub fn parse() -> Self {
        Parser::parse()
    }
}

#[derive(Debug, Clone)]
pub struct ContextVersion {
    pub major: u8,
    pub minor: u8,
}

impl ContextVersion {
    fn parse(s: &str) -> Result<Self, clap::Error> {
        let mut versions = s.split('.');

        let major = versions
            .next()
            .and_then(|x| x.parse::<u8>().ok())
            .ok_or_else(|| clap::Error::new(ErrorKind::InvalidValue))?;

        let minor = versions
            .next()
            .and_then(|x| x.parse::<u8>().ok())
            .ok_or_else(|| clap::Error::new(ErrorKind::InvalidValue))?;

        if let Some(next) = versions.next() {
            let mut err = clap::Error::new(ErrorKind::TooManyValues);

            let context = ContextValue::String(format!("{next}..."));
            err.insert(ContextKind::TrailingArg, context);

            return Err(err);
        }

        match (major, minor) {
            (3, 3) | (4, 0..=6) => Ok(Self { major, minor }),

            (1, 0..=5) | (2, 0..=1) | (3, 0..=2) => {
                let mut err = clap::Error::new(ErrorKind::ValueValidation);

                let context = ContextValue::String(format!("{major}.{minor}"));
                err.insert(ContextKind::InvalidValue, context);

                Err(err)
            }

            _ => Err(clap::Error::new(ErrorKind::InvalidValue)),
        }
    }
}

#[derive(Clone)]
struct ContextVersionParser;

impl TypedValueParser for ContextVersionParser {
    type Value = ContextVersion;

    fn parse_ref(
        &self,
        cmd: &clap::Command,
        arg: Option<&clap::Arg>,
        value: &std::ffi::OsStr,
    ) -> Result<Self::Value, clap::Error> {
        let value_str = value.to_string_lossy();

        ContextVersion::parse(&value_str).map_err(|mut err| {
            if let Some(arg) = arg {
                err.insert(
                    ContextKind::InvalidArg,
                    ContextValue::String(arg.to_string()),
                );
            }
            err.with_cmd(cmd)
        })
    }
}

#[derive(Debug, Clone)]
pub struct ScreenSize {
    pub width: u32,
    pub height: u32,
}

impl ScreenSize {
    fn parse(s: &str) -> Result<Self, clap::Error> {
        let mut sizes = s.split('x');

        let width = sizes
            .next()
            .and_then(|x| x.parse::<u32>().ok())
            .ok_or_else(|| clap::Error::new(ErrorKind::InvalidValue))?;

        let height = sizes
            .next()
            .and_then(|x| x.parse::<u32>().ok())
            .ok_or_else(|| clap::Error::new(ErrorKind::InvalidValue))?;

        if let Some(next) = sizes.next() {
            let mut err = clap::Error::new(ErrorKind::TooManyValues);

            let context = ContextValue::String(format!("{next}..."));
            err.insert(ContextKind::TrailingArg, context);

            return Err(err);
        }

        Ok(Self { width, height })
    }
}

#[derive(Clone)]
struct ScreenSizeParser;

impl TypedValueParser for ScreenSizeParser {
    type Value = ScreenSize;

    fn parse_ref(
        &self,
        cmd: &clap::Command,
        arg: Option<&clap::Arg>,
        value: &std::ffi::OsStr,
    ) -> Result<Self::Value, clap::Error> {
        let value_str = value.to_string_lossy();
        ScreenSize::parse(&value_str).map_err(|mut err| {
            if let Some(arg) = arg {
                err.insert(
                    ContextKind::InvalidArg,
                    ContextValue::String(arg.to_string()),
                );
            }
            err.with_cmd(cmd)
        })
    }
}
