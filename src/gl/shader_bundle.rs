use std::{
    fs::File,
    io::{Read, Seek},
};

use crate::gl::{program::*, shader::*};

#[derive(Debug, Clone, Copy)]
pub enum ShaderBundleError {
    ShaderCompileError(ShaderType),
    ProgramLinkFailure,
}

impl From<ShaderCompileError> for ShaderBundleError {
    fn from(value: ShaderCompileError) -> Self {
        Self::ShaderCompileError(value.0)
    }
}

impl From<ProgramLinkFailure> for ShaderBundleError {
    fn from(_: ProgramLinkFailure) -> Self {
        Self::ProgramLinkFailure
    }
}

#[derive(Debug, Default, Clone)]
struct UniformData {
    delta: f32,
    elapsed: f32,
    mouse: (i32, i32),
    screen: (i32, i32),
}

#[derive(Debug)]
pub struct ShaderBundle {
    vertex: Shader,
    fragment: Shader,
    program: Program,
    data: UniformData,
    source_buffer: String,
}

impl ShaderBundle {
    pub fn new(vertex_source: &str, fragment_source: &str) -> Result<Self, ShaderBundleError> {
        let vertex = Shader::new(ShaderType::Vertex, vertex_source)?;
        let fragment = Shader::new(ShaderType::Fragment, fragment_source)?;
        let program = Program::new(&[&vertex, &fragment])?;

        program.bind();

        Ok(Self {
            vertex,
            fragment,
            program,
            data: UniformData::default(),
            source_buffer: String::new(),
        })
    }

    pub fn hot_reload(&mut self, file: &mut File, stype: ShaderType) {
        let read_size = file.read_to_string(&mut self.source_buffer);

        if read_size.is_ok() {
            self.update_shader(stype);
        }

        let _ = file.seek(std::io::SeekFrom::Start(0));
        self.source_buffer.clear();
    }

    fn update_shader(&mut self, stype: ShaderType) {
        let new_shader = Shader::new(stype, &self.source_buffer);

        let Ok(shader) = new_shader else {
            log::warn!("Unable to rebuild shader bundle due to shader compile errors\n");
            return;
        };

        // perhaps some day the vertex shader will be able to be changed
        let (kept_shader, replaced_shader) = match stype {
            ShaderType::Vertex => (&self.fragment, &mut self.vertex),
            ShaderType::Fragment => (&self.vertex, &mut self.fragment),
        };

        let new_program = Program::new(&[&shader, kept_shader]);

        let Ok(program) = new_program else {
            log::warn!("Unable to rebuild shader bundle due to program linking errors\n");
            return;
        };

        log::info!(
            "Succesfully rebuilt shader bundle\n\t- New {:?} shader id: {}\n\t- New program id: {}\n",
            stype,
            shader.handle,
            program.handle,
        );

        self.program.unbind();

        *replaced_shader = shader;
        self.program = program;

        self.program.bind();
        self.reset();
    }

    pub fn set_delta(&mut self, delta: f32) {
        self.data.delta = delta;
        unsafe { self.program.set_1f32(b"delta\0", delta) };
    }

    pub fn set_elapsed(&mut self, elapsed: f32) {
        self.data.elapsed = elapsed;
        unsafe { self.program.set_1f32(b"elapsed\0", elapsed) };
    }

    pub fn set_mouse(&mut self, x: i32, y: i32) {
        self.data.mouse = (x, y);
        unsafe { self.program.set_2i32(b"mouse\0", x, y) };
    }

    pub fn set_screen(&mut self, width: i32, height: i32) {
        self.data.screen = (width, height);
        unsafe { self.program.set_2i32(b"screen\0", width, height) };
    }

    fn reset(&self) {
        unsafe {
            self.program.set_1f32(b"delta\0", self.data.delta);
            self.program.set_1f32(b"elapsed\0", self.data.elapsed);
            self.program
                .set_2i32(b"mouse\0", self.data.mouse.0, self.data.mouse.1);
            self.program
                .set_2i32(b"screen\0", self.data.screen.0, self.data.screen.1);
        };
    }
}
