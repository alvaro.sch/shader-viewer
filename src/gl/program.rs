use gl::types::GLuint;

use super::Shader;

#[derive(Debug, Clone, Copy)]
pub struct ProgramLinkFailure;

#[derive(Debug)]
pub struct Program {
    pub handle: GLuint,
}

impl Program {
    pub fn new(shaders: &[&Shader]) -> Result<Self, ProgramLinkFailure> {
        let id = unsafe { program_from_shaders(shaders)? };

        Ok(Self { handle: id })
    }

    pub fn bind(&self) {
        unsafe { gl::UseProgram(self.handle) };
    }

    pub fn unbind(&self) {
        unsafe { gl::UseProgram(0) };
    }

    /// # Safety
    ///
    /// name must be nul-terminated.
    pub unsafe fn set_1f32(&self, name: &[u8], f1: f32) {
        let location = gl::GetUniformLocation(self.handle, name.as_ptr().cast());

        if location != -1 {
            gl::Uniform1f(location, f1);
        }
    }

    /// # Safety
    ///
    /// name must be nul-terminated.
    pub unsafe fn set_2i32(&self, name: &[u8], i1: i32, i2: i32) {
        let location = gl::GetUniformLocation(self.handle, name.as_ptr().cast());

        if location != -1 {
            gl::Uniform2i(location, i1, i2);
        }
    }
}

impl Drop for Program {
    fn drop(&mut self) {
        self.unbind();
        unsafe { gl::DeleteProgram(self.handle) }
    }
}

unsafe fn program_from_shaders(shaders: &[&Shader]) -> Result<GLuint, ProgramLinkFailure> {
    let id = gl::CreateProgram();

    for shader in shaders {
        gl::AttachShader(id, shader.handle);
    }

    gl::LinkProgram(id);

    let mut link_status = 1;
    gl::GetProgramiv(id, gl::LINK_STATUS, &mut link_status);

    if link_status == 0 {
        let mut log_size = 0;
        gl::GetProgramiv(id, gl::INFO_LOG_LENGTH, &mut log_size);

        let mut info_log = vec![0u8; log_size as usize];
        gl::GetProgramInfoLog(
            id,
            log_size as _,
            &mut log_size,
            info_log.as_mut_ptr().cast(),
        );

        info_log.truncate(log_size as _);

        let error_message = String::from_utf8(info_log).unwrap();
        log::error!("Error while building program:\n{error_message}");

        gl::DeleteProgram(id);

        return Err(ProgramLinkFailure);
    }

    Ok(id)
}
