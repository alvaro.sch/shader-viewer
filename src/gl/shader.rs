use gl::types::{GLenum, GLuint};

#[derive(Debug, Clone, Copy)]
pub enum ShaderType {
    Vertex = gl::VERTEX_SHADER as _,
    Fragment = gl::FRAGMENT_SHADER as _,
}

#[derive(Debug, Clone, Copy)]
pub struct ShaderCompileError(pub ShaderType);

#[derive(Debug)]
pub struct Shader {
    pub handle: GLuint,
}

impl Shader {
    pub fn new(stype: ShaderType, source: &str) -> Result<Self, ShaderCompileError> {
        let id = unsafe { shader_from_source(source, stype)? };

        Ok(Self { handle: id })
    }
}

impl Drop for Shader {
    fn drop(&mut self) {
        unsafe { gl::DeleteShader(self.handle) }
    }
}

unsafe fn shader_from_source(
    source: &str,
    stype: ShaderType,
) -> Result<GLuint, ShaderCompileError> {
    let id = gl::CreateShader(stype as GLenum);

    gl::ShaderSource(
        id,
        1,
        &(source.as_bytes().as_ptr().cast()),
        &(source.len() as _),
    );
    gl::CompileShader(id);

    let mut compile_status = 1;
    gl::GetShaderiv(id, gl::COMPILE_STATUS, &mut compile_status);

    if compile_status == 0 {
        let mut log_size = 0;
        gl::GetShaderiv(id, gl::INFO_LOG_LENGTH, &mut log_size);

        let mut info_log = vec![0u8; log_size as usize];

        gl::GetShaderInfoLog(id, log_size, &mut log_size, info_log.as_mut_ptr().cast());

        info_log.truncate(log_size as _);

        let error_message = String::from_utf8(info_log).unwrap();
        log::error!("Error compiling {stype:?} shader:\n{error_message}");

        gl::DeleteShader(id);

        return Err(ShaderCompileError(stype));
    }

    Ok(id)
}
