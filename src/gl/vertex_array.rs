use gl::types::GLuint;

#[derive(Debug, Clone, Copy)]
pub enum DrawPrimitive {
    Points = gl::POINTS as _,
    Lines = gl::LINES as _,
    LineStrip = gl::LINE_STRIP as _,
    Triangles = gl::TRIANGLES as _,
    TriangleStrip = gl::TRIANGLE_STRIP as _,
}

#[derive(Debug)]
pub struct VertexArray {
    pub handle: GLuint,
}

impl VertexArray {
    #[allow(clippy::new_without_default)]
    pub fn new() -> Self {
        let mut id = 0;
        unsafe { gl::GenVertexArrays(1, &mut id) };

        Self { handle: id }
    }

    pub fn bind(&self) {
        unsafe { gl::BindVertexArray(self.handle) }
    }

    pub fn unbind(&self) {
        unsafe { gl::BindVertexArray(0) }
    }

    pub fn draw_arrays(&self, primitive: DrawPrimitive, vertices: std::ops::Range<u32>) {
        unsafe {
            gl::DrawArrays(
                primitive as _,
                vertices.start.try_into().unwrap(),
                vertices.len().try_into().unwrap(),
            )
        };
    }
}

impl Drop for VertexArray {
    fn drop(&mut self) {
        self.unbind();
        unsafe { gl::DeleteVertexArrays(1, &self.handle) }
    }
}
