#version 330

vec2 positions[4] = vec2[4](
    vec2(-1.0, -1.0),
    vec2( 1.0, -1.0),
    vec2( 1.0,  1.0),
    vec2(-1.0,  1.0)
);

int indices[6] = int[6](0, 1, 2, 0, 2, 3);

void main() {
    gl_Position = vec4(positions[indices[gl_VertexID]], 0.0, 1.0);
}
