#version 330

uniform float delta;
uniform float elapsed;
uniform ivec2 screen;
uniform ivec2 mouse;

layout(location = 0) out vec4 frag_color;

void main() {
    vec2 uv = gl_FragCoord.xy / vec2(screen);
    float t = 0.5 + 0.5 * cos(2.0 * elapsed);

    vec3 color = mix(vec3(1.0), vec3(uv, 1.0), t);

    frag_color = vec4(color, 1.0);
}
