use std::time::{Duration, Instant};

use sdl2::{
    event::{Event, WindowEvent},
    keyboard::Scancode,
    sys::SDL_WindowFlags,
    video::{GLContext, GLProfile, Window},
};

use crate::{
    cli,
    gl::{self, shader::ShaderType, vertex_array::DrawPrimitive, ShaderBundle},
    FileWatcher,
};

pub struct App {
    window: Window,
    _context: GLContext,
    watcher: FileWatcher,
    bundle: ShaderBundle,
    should_quit: bool,
}

impl App {
    pub fn new(args: &cli::Args) -> Self {
        let sdl_context = sdl2::init().expect("unable to initialize platform API");

        let window = Self::create_window(args, &sdl_context);

        let context = Self::create_gl_context(args, &window);

        let watcher = Self::create_file_watcher(args);

        let bundle = Self::create_shader_bundle();

        Self {
            window,
            watcher,
            bundle,
            should_quit: false,
            _context: context,
        }
    }

    //
    // initialization
    //

    fn create_window(args: &cli::Args, sdl_context: &sdl2::Sdl) -> Window {
        let video = sdl_context
            .video()
            .expect("unable to initialize video subsystem");

        let gl_attr = video.gl_attr();
        gl_attr.set_context_version(args.context_version.major, args.context_version.minor);
        gl_attr.set_context_profile(GLProfile::Core);
        gl_attr.set_double_buffer(true);

        let width = args.size.width;
        let height = args.size.height;

        let window_flags = SDL_WindowFlags::SDL_WINDOW_RESIZABLE as u32
            | SDL_WindowFlags::SDL_WINDOW_HIDDEN as u32
            | SDL_WindowFlags::SDL_WINDOW_OPENGL as u32
            | if args.always_on_top {
                SDL_WindowFlags::SDL_WINDOW_ALWAYS_ON_TOP as u32
            } else {
                0
            };

        video
            .window(&args.window_title, width, height)
            .set_window_flags(window_flags)
            .build()
            .expect("unable to create window")
    }

    fn create_gl_context(args: &cli::Args, window: &Window) -> GLContext {
        let gl_context = window
            .gl_create_context()
            .expect("unable to create opengl context");

        gl::load(window);
        gl::viewport(0, 0, args.size.width as i32, args.size.height as i32);

        gl_context
    }

    fn create_file_watcher(args: &cli::Args) -> FileWatcher {
        let watch_interveal = Duration::new(args.time_interveal, 0);
        FileWatcher::new(args.watch.as_ref(), watch_interveal).unwrap()
    }

    fn create_shader_bundle() -> ShaderBundle {
        let vertex_source = include_str!("shaders/builtin.vert");
        let fragment_source = include_str!("shaders/builtin.frag");
        ShaderBundle::new(vertex_source, fragment_source).unwrap()
    }

    //
    // main loop
    //

    pub fn run(mut self) {
        let mut event_pump = self
            .window
            .subsystem()
            .sdl()
            .event_pump()
            .expect("unable to initalize event subsystem");

        if let Some(file) = self.watcher.file_handle() {
            self.bundle.hot_reload(file, ShaderType::Fragment);
        }

        let vao = gl::VertexArray::new();
        vao.bind();

        let startup = Instant::now();
        let mut t0 = startup;

        self.window.show();

        while !self.should_quit {
            //
            // events
            //

            event_pump
                .poll_iter()
                .for_each(|event| self.handle_event(event));

            //
            // update
            //

            let t1 = Instant::now();

            let delta = t1.duration_since(t0).as_secs_f32();
            let elapsed = t1.duration_since(startup).as_secs_f32();
            t0 = t1;

            self.bundle.set_delta(delta);
            self.bundle.set_elapsed(elapsed);

            let file_updated = self.watcher.file_updated();
            if file_updated {
                self.bundle
                    .hot_reload(self.watcher.file_handle().unwrap(), ShaderType::Fragment);
            }

            //
            // render
            //

            vao.draw_arrays(DrawPrimitive::Triangles, 0..6);
            self.window.gl_swap_window();
        }
    }

    fn handle_event(&mut self, event: Event) {
        match event {
            Event::Quit { .. } => self.should_quit = true,

            Event::KeyDown {
                scancode: Some(Scancode::Escape),
                ..
            } => self.should_quit = true,

            Event::Window {
                win_event: WindowEvent::Resized(width, height),
                ..
            } => {
                gl::viewport(0, 0, width, height);
                self.bundle.set_screen(width, height);
            }

            Event::DropFile { filename, .. } => {
                if self.watcher.swap_file(&filename).is_ok() {
                    self.bundle
                        .hot_reload(self.watcher.file_handle().unwrap(), ShaderType::Fragment);
                }
            }

            Event::MouseMotion { x, y, .. } => self.bundle.set_mouse(x, y),

            _ => {}
        }
    }
}
